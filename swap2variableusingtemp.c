#include<stdio.h>
int main()
{
    int x,y,temp;
    printf("Enter 2 numbers to swap \n");
    scanf("%d%d",&x,&y);
    printf("Before swapping x=%d & y=%d,\n",x,y);
    temp=x;
    x=y;
    y=temp;
    printf("\nAfter swapping x=%d & y=%d\n",x,y);
    return 0;
}