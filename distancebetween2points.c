#include <stdio.h>
#include <math.h>
int main ()
{
    float x1,y1,x2,y2,d;
    printf ("Enter co-ordinate of first point\n");
    scanf ("%f%f",&x1,&y1);
    printf ("Enter co-ordinate of second point\n");
    scanf ("%f%f",&x2,&y2);
    d=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    printf ("The distance between point 1 and point 2 is %f",d);
    return 0;
}